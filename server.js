'use strict'

const express = require('express');
const app = express();
const cors = require('cors');
const port = process.env.PORT || 6000;
const detailsRoutes = require("./routes/sample");
const path = require("path");
const swaggerUi = require('swagger-ui-express');

// Database connection
const db = require('./config/config');

db.authenticate().then(()=>{
    console.log("Database connected");
}).catch((err)=>{
    console.log("Error" + err);
});

app.use(cors());
app.use(express.json());
// require("./public")

const options = {
    explorer: true,
    customSiteTitle: 'ReSuit - Rental Service',
    swaggerOptions: {
        urls: [
            {
                url: './public/swagger/crud.json',
                name: "All Users APIs"
            }
        ]
    }
};

// require('./')

app.use("./public", express.static(path.join(__dirname, 'public')));
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(null, options));

app.use('/api/v1/details', detailsRoutes);

// server connection
db.sync().then(()=>{
    app.listen(port, console.log(`listing port ${port}`));
}).catch((err)=>{
    console.log("Connection failed" + err);
});